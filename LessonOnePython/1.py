def count_decorator(func):
    def inner():
        global count
        count += 1
        func_return = func()
        if count >= 11 and count <= 20 or count % 100 >= 11 and count % 100 <= 20:
            print("Функция была вызвана", count, "раз")
        elif count == 1 or count % 10 == 1:
            print("Функция была вызвана", count, "раз")
        elif count >= 2 and count <= 4 or count % 10 >= 2 and count % 10 <= 4:
            print ("Функция была вызвана", count, "разa")
        else:
            print("Функция была вызвана", count, "раз")

        return func_return
    return inner

count = 0

@count_decorator
def test_func():
    pass

for i in range(23):
    test_func()
