# В виде списка [name, gender, age, weight, count_of_pushups, count_of_pullups, run_in_minutes_5km]
ivan = ["Ivan Ivanov", "male", 19, 65, 40, 16, 14.06]
nastya = ["Nastya Tihan", "female", 21, 51, 25, 8, 13.54]
james = ["James Chainikov", "male", 18, 76, 43, 20, 15.13]

# В виде класса

class Athlete:
    def __init__(self, name, gender, age, weight, count_of_pushups, count_of_pullups, run_in_minutes_5km):
        self.name = name
        self.gender = gender
        self.age = age
        self.weight = weight
        self.count_of_pushups = count_of_pushups
        self.count_of_pullups = count_of_pullups
        self.run_in_minutes_5km = run_in_minutes_5km



