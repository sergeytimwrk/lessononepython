def isPalindrom(word):
    for i in range(len(word) // 2):
        if word[i] != word[-i]:
            return False
    return True

input_word = input()
if isPalindrom(input_word):
    print(input_word, "является палиндром")
else:
    print(input_word, "не является палиндромом")