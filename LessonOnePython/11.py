from datetime import date

def date_func(year, month, day):
  try:
    date(year, month, day)
    return True
  except:
    return False

print(date_func(2022, 7, 4))
print(date_func(2022, 7, 32))