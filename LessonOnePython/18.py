'''
class Athlete:
      pass
Athlete()
print(Athlete())
#<__main__.Athlete object at 0x0000023BBAB38FD0>

a = Athlete()
b = Athlete()
print(a == b)
#False
'''
import random


class Athlete:

    def __init__(self, name, gender, age, weight, count_of_pushups, count_of_pullups, run_in_minutes_5km):
        self.name = name
        self.gender = gender
        self.age = age
        self.weight = weight
        self.count_of_pushups = count_of_pushups
        self.count_of_pullups = count_of_pullups
        self.run_in_minutes_5km = run_in_minutes_5km

    def __str__(self):
        return f"\nИмя: {self.name} \nПол: {self.gender} \nВозраст: {self.age} \nВес: {self.weight} \nКоличество отжиманий: {self.count_of_pushups} \nКоличество подтягиваний: {self.count_of_pullups} \nБег 5км в минутах: {self.run_in_minutes_5km}"

    def print_info(self):
        print(f"\nИмя: {self.name} \nПол: {self.gender} \nВозраст: {self.age} \nВес: {self.weight}",
              f"Количество отжиманий: {self.count_of_pushups} \nКоличество подтягиваний: {self.count_of_pullups} \nБег 5км в минутах: {self.run_in_minutes_5km}", sep='\n')

    def victory_word(self, word):
        if random.randint(0, 1):
            print(f"\nРазгромив своих соперников, {self.name} выкрикивает свои победные слова: {word}!")
        else:
            print(f"\nФинишируя, {self.name} обращается к публике словами: {word}!")


class Champion(Athlete):
    category = "Чемпион"

    def victory_word(self, word):
        print(f"\nЯ чемпион и я никогда не сдам свои позиции! {word}")


class OrdinaryAthlete(Athlete):
    category = "Обычный атлет"


class UnluckyAthlete(Athlete):
    category = "Атлет с черной полосой"

    def victory_word(self, word):
        return super().victory_word(f"Наконец-то повезло! {word}")

'''
Athlete()
print(Athlete())
#Traceback (most recent call last):
#  File "G:\Projects\Python\Main\main.py", line 23, in <module>
#   Athlete()
#TypeError: __init__() missing 7 required positional arguments: 'name', 'gender', 'age', 'weight', 'count_of_pushups', 'count_of_pullups', and 'run_in_minutes_5km'
'''

ivan_oop = OrdinaryAthlete("Ivan Ivanov", "male", 17, 65, 40, 16, 14.06)
nastya_oop = OrdinaryAthlete("Nastya Tihan", "female", 17, 51, 25, 8, 13.54)
james_oop = OrdinaryAthlete("James Chainikov", "male", 18, 76, 43, 20, 15.13)

print(f"Вес  {ivan_oop.name}: {ivan_oop.weight}, возраст: {ivan_oop.age}", f"\nКатегория: {ivan_oop.category}")

ivan_oop.age = 20
ivan_oop.category = "Почти Чемпион"
print(f"\nКоличество отжиманий {ivan_oop.name}: {ivan_oop.count_of_pushups},  возраст: {ivan_oop.age}", f"\nКатегория: {ivan_oop.category}")

nastya_oop.print_info()

james_oop.victory_word("Спасибо вам, друзья")
ivan_oop.victory_word("Да, я сделал это! Благодарю вас за поддержку")

print(ivan_oop)

ilya_oop = Champion("Ilya Snezkov", "male", 17, 60, 60, 30, 11.06)
ilya_oop.victory_word("Спасибо!")

zhenya_oop = UnluckyAthlete("Zhenya Ronov", "male", 17, 78, 15, 3, 20.65)
zhenya_oop.victory_word("Ура!")
