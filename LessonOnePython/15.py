def sum_range(a, z):
    if a > z:
        tmp = a
        a = z
        z = tmp
    our_sum = 0
    for i in range (int(a), int(z+1)):
        our_sum += i
    return our_sum

print(sum_range(1.8, 10))
print(sum_range(10, 1.6))